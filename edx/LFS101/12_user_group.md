# User and Group

```bash
whoami
who -a

usermod --help #command to modify user account (homedir, add to groups, ...)
```

First login

1. ~/.bash_profile
2. ~/.bash_login
3. ~/.profile (login shell)


New shell
1. ~/.bashrc (interactive shell)

## Aliases
Placed in ~/.bashrc

```bash
alias #list of aliases
unalias 

alias conn_domainw='ssh root@domain.com'
```

## User

```bash
id #uid: unique id by user
gui # unique id by group

#/etc/passwd
#/etc/group

sudo useradd -m -c "John Doe" -s /bin/bash johndoe #-m create home dir
sudo passwd johndoe
cat /etc/skel #skeleton for home directory new user
sudo /usr/sbin/useradd new_user (opensuse)

sudo userdel -r new_user #-r remove home directory

```

## Group
```bash
sudo /usr/sbin/groupadd anewgroup
sudo /usr/sbing/groupdel anewgroup

usermod -aG sudo username #add to sudoers
```

## sudo vs su

```bash
su  #become root temporarily

#/etc/sudoers
```

## Environment variables
```bash
env #list env variables

echo $SHELL
export VARIABLE=value (or VARIABLE=value; export VARIABLE)

echo export VARIABLE=value >> ~/.bashrc
source ~/.bashrc (or $SHELL)


SDIRS=s_0* KROOT=/lib/modules/$(uname -r)/build make modules_install # feeds values of SDIRS and KROOT env var to command make modules_install

$HOME, $SHELL,
export PATH=$HOME/bin:$PATH
echo $PATH #/home/student/bin:/usr/local/bin:/usr/bin:/bin/usr

echo $PS1
\u - user name
\h - host name
\w - current wd
\! - history number of this command
\d - date

~/bash_history
history
HISTFILE
HISTFILESIZE
HISTSIZE
HISTCONTROL
HISTIGNORE

CTRL + R #search previous commands

history
1. echo $SHELL
2. ls -a
3. sleep 1000

!1 #execute echo $SHELL
!sl #execute the command beginning with sl
```

| keyboard shortcut | Task |
| --- | --- |
| CTRL-L | clear screen |
| CTRL-D | exits current shell |
| CTRL-Z | puts current process in bg |
| CTRL-C | kill current process |
| CTRL-H | works as backspace |
| CTRL-A | goes to beginning of the line | 
| CTRL-W | delete word before cursor | 
| CTRL-U | deletes from beginning of line to cursor position | 
| CTRL-E | Goes to the end of the line | 
| Tab | autocompletes |


## File ownership

```bash
chown #change user ownership
chgrp #change group ownership
chmod #change file permission

chmod uo+x,g-w somefile

```