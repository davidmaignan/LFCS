# Networking

```bash
cat /etc/hosts
cat /etc/resolv.conf

host linuxfoundation.org #hosts - static table lookup for hostnames
nslookup linuxfoundation.org #query Internet name servers interactively
dig linuxfoundation.org #DNS lookup utility
```

## Configuration files

```bash
# Debian
/etc/network


#Fedora, suse
/etc/sysconfig/network

nmtui #utility to configure network config
nmcli #

ip addr|link
ip -4 addr
ping linuxfoundation.org

route -n #deprecated

traceroute

```

### Tunneling

```bash
ssh -N david@192.168.2.56 -L:8080:192.168.2.56:3000 #-N do not execute a remote command
 ```

| tools | Description |
|---|---|
| ethtool | queries network interfaces. set params (speed) |
| netstat | display active connections and routing table. Monitoring performance and troubleshooting |
| nmap | scan open ports on a network |
| tcpdump | dump network traffic |
| iptraf | monitor traffic |
| mtr | ping + traceroute |
| dig | test dns (replace host & nslookup) |

```bash

ifdown eth0
ifup eth0

ethtool -s eth0 speed 100 autoneg off #change speed 
ethtool -i eth0 # driver settings
ethtool -S eth0 # statistic
netstat -r


```

### nmap

| command | description |
| --- | --- |
| nmap -sP 192.168.1.1/24 | scan list of devices up on a given subnet |
| nmap -v 192.168.1.17 | scan well known ports on a single host |
| nmap -sS scanme.nmap.org | stealth scan (no 3rd way handshake) |
| nmap -sV scanme.nmap.org | stealth + version |
| nmap -A ... | agressive scanning |
| nmap 192.168.1.* 192.168.1,2,3 192.186.1.2-45| scan subnet at once |
| nmap -p T:7777, 973, 76-895 scanme.nmap.org | port scanning |


### Browser

- Lynx
- Elinks
- w3m

```bash
wget <url>
curl -o saved.html http://www.mysite
curl -v google.com
curl --head google.com

ssh -l user host
ssh user@host
scp <localfile> <user@remotesystem>:/home/user/ 
```
