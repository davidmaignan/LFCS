# Text manipulation

```bash
cat
tac
head
tail
zcat
zless
zmore
zgrep 

sed s/pattern/replace/ file #substitute first occurence everyline
sed s/.../../g #all occurences
sed 1,3s/pattern/.../g flile #sub all string in a range of lines
sed -i .... #save changes in same file
sed -e pattern1 -e pattern2 ...
sed -e 's/01/JAN/' -e 's/02/FEB/' -e 's/03/MAR/' -e 's/04/APR/' -e 's/05/MAY/' \
    -e 's/06/JUN/' -e 's/07/JUL/' -e 's/08/AUG/' -e 's/09/SEP/' -e 's/10/OCT/' \
    -e 's/11/NOV/' -e 's/12/DEC/'

awk -F: '{print "name: "$1"  shell: " $7}' /etc/passwd

```

#### Paste
paste can be used to combine fields (such as name or phone number) from different files, as well as combine lines from multiple files. 

```bash
paste file1 file2 -d, #paste line by line with commat separated

```

#### join
 It first checks whether the files share common fields, such as names or phone numbers, and then joins the lines in two files based on a common field.

#### Split

```bash
split -n 100000 words_split.txt  wd_split -d --additional-suffix .txt
```

#### grep
```bash

grep [0-9] -v -C 3 filename #-v: inverse -C context (3 lignes) 

string filename # extract all printable characters
tr # replace / delete character 
cat file1 | tr -s ' ' #squeeze multispaces to one
ls -l | tee newfile #takes output from std to save to file
```




