# Text editor

## Create file
```bash

cat << EOF >> myfile
> line one
> line two
> EOF

```

## Nano
- CTRL - G: display help
- CTRL - X: exit a file
- CTRL - C: cancels
...


## Gedit
a simple-to-use graphical editor that can only be run within a Graphical Desktop environment. It is visually quite similar to the Notepad text editor in Windows, but is actually far more capable and very configurable and has a wealth of plugins available to extend its capabilities further.

```bash
gedit filename.txt
```

## vi
