# Shell scripting

```bash

echo $? #return value by previous process

make; make install; make clean
make && make install && make clean
cat file1 || cat file2 || cat file3

ls /lib/modules/$(uname -r)/ # command substitution

```

- compiled applications: rm, ls, df, vi, gzip, ...
- built-in bash commands: cd, pwd, echo ,read, logout, printf, let, ulimit, ...

help: get list of commands

| parameter | meaning |
| --- | --- | 
| $0 | script name |
| $1 | first param |
| $2,$3,... | all parameters |
| $* | all parameters |
| $# | number of arguments |


### env

```bash
env, set, printenv

VAR=value ; export VAR or export VAR=value
```